import React from 'react'
import '../layout/Layout.scss'

const Home = () => (
  <div className="MainPage">
    <div className="header">
      <img src="/static/georgie-cobbs-459520-unsplash.png" alt="Georgie cobbs" />
    </div>
    <div className="mainContent">
    This is mainContent
    </div>
    <div className="footer">
    This is footer
    </div>
  </div>
)

export default Home
